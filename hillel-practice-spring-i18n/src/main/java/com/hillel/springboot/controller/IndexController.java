package com.hillel.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

@Controller
@RequestMapping("/")
public class IndexController {

    @GetMapping
    public String getHomePage(Model model, HttpServletRequest request) {
        //model.addAttribute(name, value);

        Locale currentLocale = request.getLocale();
        String currentLanguege = currentLocale.getLanguage();
        switch (currentLanguege){
            case "ua": return "redirect:/?locale=en";
            case "es": return "redirect:/?locale=es";
            case "it": return "redirect:/?locale=it";
        }
        return "index";
    }
}
